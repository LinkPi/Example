#include <QApplication>
#include "Link.h"
#include "../VIO/interface.h"
#include "Json.h"

int main(int argc, char *argv[])
{
    argv[argc++]="-platform";
    argv[argc++]="offscreen";
    QApplication a(argc, argv);
    Link::init();

    LinkObject *vi=Link::create("InputVi");
    QVariantMap dataVi;
    dataVi["interface"]=INTERFACE_VIDEO;
    vi->start(dataVi);

    LinkObject *overlay=Link::create("Overlay2");
    QVariantMap dataOver;
    QVariantList list=Json::loadFile("/link/config/misc/overlayDemo.json").toList();
    QVariantList list2;
    for(int i=0;i<list.count();i++)
    {
        QVariantMap map=list[i].toMap();
        map["enable"]=true;
        list2<<map;
    }
    dataOver["lays"]=list2;
    overlay->start(dataOver);

    LinkObject *vo=Link::create("OutputVo");
    QVariantMap dataVo;
    dataVo["type"]="hdmi";
    vo->start(dataVo);

    vi->linkV(overlay)->linkV(vo);

    return a.exec();
}
