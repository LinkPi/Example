TEMPLATE = subdirs


DISTFILES += \
    build.pri

SUBDIRS += \
    VIO \
    AIO \
    Stream \
    Record \
    Decode \
    GPIO \
    Snap \
    USBCam \
    Mix \
    UI \
    DIY \
    Overlay \
    OLED \
    PowerCtrl \
    LowLatencyENC \
    LowLatencyDEC \
    FileTranscode \
    StreamTranscode \
    Player \
    AEC \
    RecordSegment \
    Crop \
    SimpleEncoder \
    Overlay2
